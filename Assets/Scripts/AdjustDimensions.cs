/*
	© 2016 Goblinsama S.r.l.s., All rights reserved
*/

using UnityEngine;

namespace Nether_Map
{
	[ExecuteInEditMode]
	public class AdjustDimensions : MonoBehaviour
	{
		[SerializeField] private Vector2 min;
		[SerializeField] private Vector2 max;
		[SerializeField] private bool UPDATE;
		
		void Update ()
		{
			if (UPDATE)
			{
				float centerX = (min.x + max.x)/2f;
				float centerZ = (min.y + max.y)/2f;
				float sizeX = Mathf.Abs(min.x - max.x) + 1;
				float sizeZ = Mathf.Abs(min.y - max.y) + 1;
				
				transform.position = new Vector3(centerX, 0, centerZ);
				transform.localScale = new Vector3(sizeX, 1, sizeZ);
				
				UPDATE = false;
			}
		}
	}
}
